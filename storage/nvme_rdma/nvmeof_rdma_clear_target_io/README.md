# storage/nvme_rdma/nvmeof_rdma_clear_target_io

Storage: nvmeof rdma clear target during io

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
